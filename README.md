# Journal - Basic Logging support for Go projects  
  
## Purpose
Initialize logging for DEBUG, INFO, WARNING, and/or ERROR messages.  See "Usage" below.  
  
### Install GO Locally ###
Make sure you have [Go](https://golang.org/) installed, and make sure your `$GOPATH` is configured correctly.  Then...  
  
### Tweak Local GIT Config ###
Please read [this URL](https://edenmal.moe/post/2017/Golang-go-get-from-Gitlab/) to configure your local **git** instance.  
  
```bash
go get gitlab.oit.duke.edu/dul-go/journal
```
*Note* - Make sure you have the necessary keys in place (SSH, GPG, etc).  

## USAGE
### Example 1 - Configure all four options:
```go
package myapp

import (
    "fmt"
    "gitlab.oit.duke.edu/dul-go/journal"
)

func main() {
    fmt.Println("Hello world")
    
    journal.InitLogging(os.Stdout,  // DEBUG
                        os.Stdout,  // INFO
                        os.Stdout,  // WARNING
                        os.Stderr   // ERROR
                        )
    fmt.Println("Logging initialized...")
}
```
  
### Example 2 - Ignore TRACE (using ioutil.Discard)
```go
package myotherapp

import (
    "fmt"
    "io/ioutil"
    "gitlab.oit.duke.edu/dul-go/journal"
)

func main() {
    fmt.Println("Hello world")
    
    journal.InitLogging(ioutil.Discard, // DEBUG (no logging support here)
                        os.Stdout,      // INFO
                        os.Stdout,      // WARNING
                        os.Stderr       // ERROR
                        )
    fmt.Println("Logging initialized...")
}
```
  
## TODO
Include individual initializer functions for DEBUG, INFO, WARNING, ERROR -- with possible "log" specifiers (eg. log.Ltime...)